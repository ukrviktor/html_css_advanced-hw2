
const {src, dest, watch, series, parallel} = require("gulp");

// Плагіни
const browserSync = require("browser-sync").create();
const del = require("del");
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
let rename = require("gulp-rename");
const fileInclude = require("gulp-file-include");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const concat = require("gulp-concat");
const debug = require("gulp-debug");
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
let uglify = require('gulp-uglify-es').default;


//Підключення файлів html в index.html
function buildPages () {
    return src('./src/index.html')
    .pipe(fileInclude())
        .pipe(dest('./dist'))
        .pipe(browserSync.stream());
}
//Обробка scss css
function buildStyles() {
    return src('./src/scss/**/main.scss')
    // sourcemaps
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: notify.onError()}))
    .pipe(debug({title: 'Src'}))
    // Конверт в css
    .pipe(sass().on('error', sass.logError))
    .pipe(debug({title: 'Css'}))
    // autoprefixer
    .pipe(autoprefixer({
        cascade: false
    }))
    // Збірка в одни файл
    .pipe(concat('styles.min.css'))
    .pipe(cleanCSS())
    .pipe(debug({title: 'Concat'}))
    // sourcemaps
    .pipe(sourcemaps.write())
    // destination
    .pipe(dest('./dist/css'))
    .pipe(browserSync.stream());

}
//Обробка js
function buildScripts() {
	return src('./src/js/**/*.js')
		// sourcemaps
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(debug({title: 'Src'}))
		// all js into one
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(debug({title: 'Concat'}))
		// sourcemaps
		.pipe(sourcemaps.write())
		// destination
		.pipe(dest('./dist/js'))
		.pipe(browserSync.stream());
}


// Обробка і копіювання images 
function copyImg() {
	return src('./src/img/**/*.*')
	    .pipe(dest('./dist/img'))
		.pipe(browserSync.stream());
}


// Спостереження
function watcher(){
    watch('./src/html/**/*.html', buildPages)
    watch('./src/scss/**/*.scss', buildStyles)
    watch('./src/js/**/*.js', buildScripts)
}

// Сервер
const server = () =>{
browserSync.init({
    server:{
        baseDir: './dist'
    }
})
};

// Очищення dist
const cleanDist = () =>{
return del('./dist')
}

// Задачі

exports.buildPages = buildPages;
exports.watcher = watcher;
exports.copyImg = copyImg;
exports.cleanDist = cleanDist;
exports.buildStyles = buildStyles;
exports.buildScripts = buildScripts;

// Збірка
exports.build = series(
    cleanDist,
    parallel(buildPages, copyImg, buildStyles, buildScripts,),
    parallel(watcher,server)
);
exports.dev = series(
    parallel(buildPages, copyImg, buildStyles, buildScripts,),
    parallel(watcher,server)
);