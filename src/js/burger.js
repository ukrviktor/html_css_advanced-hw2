'use strict'

const burgerIcon = document.querySelector('.header__burger-menu--wrapper');
const burgerMenu = document.querySelector('.header__burger-menu')
burgerIcon.addEventListener('click', open)

function open() {
    burgerIcon.classList.toggle('open');
    openBurgerMenu()
}

function openBurgerMenu() {
    burgerMenu.classList.toggle('open')
}